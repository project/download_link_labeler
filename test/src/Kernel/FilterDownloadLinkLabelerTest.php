<?php

namespace Drupal\Tests\download_link_labeler\Kernel;

use Drupal\Core\Url;
use Drupal\KernelTests\KernelTestBase;

/**
 * @covers \Drupal\download_link_labeler\Plugin\Filter\FilterDownloadLinkLabeler
 * @group filter
 */
class FilterDownloadLinkLabelerTest extends KernelTestBase {

  /**
   * @var \Drupal\download_link_labeler\Plugin\Filter\FilterDownloadLinkLabeler
   */
  protected $filter;

  /**
   * The path to the module.
   */
  protected $module_path;

  /**
   * The absolute URL to the module.
   */
  protected $url;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['filter', 'download_link_labeler'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['filter', 'download_link_labeler']);
    /** @var \Drupal\Component\Plugin\PluginManagerInterface $manager */
    $manager = \Drupal::service('plugin.manager.filter');
    /** @var \Drupal\Core\Extension\ModuleHandlerInterface $handler */
    $handler = \Drupal::service('module_handler');
    $this->module_path = '/' . $handler->getModule('download_link_labeler')->getPath();
    $this->url = Url::fromUri('base:' . $this->module_path, ['absolute' => TRUE])->toString();
    $this->filter = $manager->createInstance('download_link_labeler');
    $this->filter->settings = ['included_extensions' => 'pdf doc docx ppt pptx xls xlsx zip'];
  }

  /**
   * @covers ::process
   */
  public function testProcess() {
    $tests = [
      ['<a href="' . $this->module_path . '/test/fixtures/example_1.pdf">Internal Download PDF</a>', '<a href="' . $this->module_path . '/test/fixtures/example_1.pdf">Internal Download PDF</a>&nbsp;<span class="link-label label-file label-file-pdf" aria-label="File Type and Size">pdf&nbsp;&nbsp;8.67&nbsp;KB</span>'],
      ['<a href="' . $this->url . '/test/fixtures/example_1.pdf">External Download PDF</a>', '<a href="' . $this->url . '/test/fixtures/example_1.pdf">External Download PDF</a>&nbsp;<span class="link-label label-file label-file-pdf" aria-label="File Type and Size">pdf&nbsp;&nbsp;8.67&nbsp;KB</span>'],
      ['<a href="' . $this->module_path . '/test/fixtures/example_2.doc">Internal Download DOC</a>', '<a href="' . $this->module_path . '/test/fixtures/example_2.doc">Internal Download DOC</a>&nbsp;<span class="link-label label-file label-file-doc" aria-label="File Type and Size">doc&nbsp;&nbsp;8.67&nbsp;KB</span>'],
      ['<a href="' . $this->url . '/test/fixtures/example_2.doc">External Download DOC</a>', '<a href="' . $this->url . '/test/fixtures/example_2.doc">External Download DOC</a>&nbsp;<span class="link-label label-file label-file-doc" aria-label="File Type and Size">doc&nbsp;&nbsp;8.67&nbsp;KB</span>'],
      ['<a href="' . $this->module_path . '/test/fixtures/example_3.docx">Internal Download DOCX</a>', '<a href="' . $this->module_path . '/test/fixtures/example_3.docx">Internal Download DOCX</a>&nbsp;<span class="link-label label-file label-file-docx" aria-label="File Type and Size">docx&nbsp;&nbsp;8.67&nbsp;KB</span>'],
      ['<a href="' . $this->url . '/test/fixtures/example_3.docx">External Download DOCX</a>', '<a href="' . $this->url . '/test/fixtures/example_3.docx">External Download DOCX</a>&nbsp;<span class="link-label label-file label-file-docx" aria-label="File Type and Size">docx&nbsp;&nbsp;8.67&nbsp;KB</span>'],
      ['<a href="' . $this->module_path . '/test/fixtures/example_4.ppt">Internal Download PPT</a>', '<a href="' . $this->module_path . '/test/fixtures/example_4.ppt">Internal Download PPT</a>&nbsp;<span class="link-label label-file label-file-ppt" aria-label="File Type and Size">ppt&nbsp;&nbsp;8.67&nbsp;KB</span>'],
      ['<a href="' . $this->url . '/test/fixtures/example_4.ppt">External Download PPT</a>', '<a href="' . $this->url . '/test/fixtures/example_4.ppt">External Download PPT</a>&nbsp;<span class="link-label label-file label-file-ppt" aria-label="File Type and Size">ppt&nbsp;&nbsp;8.67&nbsp;KB</span>'],
      ['<a href="' . $this->module_path . '/test/fixtures/example_5.pptx">Internal Download PPTX</a>', '<a href="' . $this->module_path . '/test/fixtures/example_5.pptx">Internal Download PPTX</a>&nbsp;<span class="link-label label-file label-file-pptx" aria-label="File Type and Size">pptx&nbsp;&nbsp;8.67&nbsp;KB</span>'],
      ['<a href="' . $this->url . '/test/fixtures/example_5.pptx">External Download PPTX</a>', '<a href="' . $this->url . '/test/fixtures/example_5.pptx">External Download PPTX</a>&nbsp;<span class="link-label label-file label-file-pptx" aria-label="File Type and Size">pptx&nbsp;&nbsp;8.67&nbsp;KB</span>'],
      ['<a href="' . $this->module_path . '/test/fixtures/example_6.xls">Internal Download XLS</a>', '<a href="' . $this->module_path . '/test/fixtures/example_6.xls">Internal Download XLS</a>&nbsp;<span class="link-label label-file label-file-xls" aria-label="File Type and Size">xls&nbsp;&nbsp;8.67&nbsp;KB</span>'],
      ['<a href="' . $this->url . '/test/fixtures/example_6.xls">External Download XLS</a>', '<a href="' . $this->url . '/test/fixtures/example_6.xls">External Download XLS</a>&nbsp;<span class="link-label label-file label-file-xls" aria-label="File Type and Size">xls&nbsp;&nbsp;8.67&nbsp;KB</span>'],
      ['<a href="' . $this->module_path . '/test/fixtures/example_7.xlsx">Internal Download XLSX</a>', '<a href="' . $this->module_path . '/test/fixtures/example_7.xlsx">Internal Download XLSX</a>&nbsp;<span class="link-label label-file label-file-xlsx" aria-label="File Type and Size">xlsx&nbsp;&nbsp;8.67&nbsp;KB</span>'],
      ['<a href="' . $this->url . '/test/fixtures/example_7.xlsx">External Download XLSX</a>', '<a href="' . $this->url . '/test/fixtures/example_7.xlsx">External Download XLSX</a>&nbsp;<span class="link-label label-file label-file-xlsx" aria-label="File Type and Size">xlsx&nbsp;&nbsp;8.67&nbsp;KB</span>'],
      ['<a href="' . $this->module_path . '/test/fixtures/example_8.zip">Internal Download ZIP</a>', '<a href="' . $this->module_path . '/test/fixtures/example_8.zip">Internal Download ZIP</a>&nbsp;<span class="link-label label-file label-file-zip" aria-label="File Type and Size">zip&nbsp;&nbsp;8.67&nbsp;KB</span>'],
      ['<a href="' . $this->url . '/test/fixtures/example_8.zip">External Download ZIP</a>', '<a href="' . $this->url . '/test/fixtures/example_8.zip">External Download ZIP</a>&nbsp;<span class="link-label label-file label-file-zip" aria-label="File Type and Size">zip&nbsp;&nbsp;8.67&nbsp;KB</span>'],
    ];
    foreach ($tests as $test) {
      $html = reset($test);
      /** @var \Drupal\filter\FilterProcessResult $actual */
      $actual = $this->filter->process($html, 'und')->getProcessedText();
      $expected = end($test);
      $this->assertSame($expected, $actual);
    }
  }

}
