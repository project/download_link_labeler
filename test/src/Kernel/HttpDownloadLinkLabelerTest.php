<?php

namespace Drupal\Tests\download_link_labeler\Kernel;

use Drupal\Core\Url;
use Drupal\download_link_labeler\Http\HttpDownloadLinkLabeler;
use Drupal\KernelTests\KernelTestBase;

/**
 * @covers \Drupal\download_link_labeler\Http\HttpDownloadLinkLabeler
 * @group filter
 */
class HttpDownloadLinkLabelerTest extends KernelTestBase {

  /**
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $handler;

  /**
   * @var \Drupal\download_link_labeler\Http\HttpDownloadLinkLabeler
   */
  protected $http;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['download_link_labeler'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->handler = \Drupal::service('module_handler');
    $this->http = new HttpDownloadLinkLabeler;
  }

  /**
   * Test get file size method.
   */
  public function testGetFileSize() {
    $module_path = $this->handler->getModule('download_link_labeler')->getPath();
    $url = Url::fromUri('base:' . $module_path . '/test/fixtures/example_1.pdf', ['absolute' => TRUE])->toString();
    $size = '8873';
    $this->assertSame($size, $this->http->getFileSize($url));
  }

}
